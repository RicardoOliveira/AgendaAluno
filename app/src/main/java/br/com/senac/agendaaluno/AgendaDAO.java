package br.com.senac.agendaaluno;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sala304b on 12/03/2018.
 */

public class AgendaDAO extends SQLiteOpenHelper{



    private static final String DATABASE = "SQLite" ;
    private static final int VERSAO = 1 ;

    public AgendaDAO(Context context) {
        super(context, DATABASE , null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String ddl = "CREATE TABLE Agenda"+
                "( id INTEGER PRIMARY KEY ," +
                "nome TEXT NOT NULL ,"+
                "telefone TEXT ,"+
                "celular TEXT ," +
                "endereco TEXT) ;" ;

        sqLiteDatabase.execSQL(ddl);


    }




    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        String ddl = "DROP TABLE IF EXISTS Agenda ;" ;
        sqLiteDatabase.execSQL(ddl);
        this.onCreate(sqLiteDatabase);

    }


    public void salvar(Agenda agenda){

        ContentValues values = new ContentValues();
        values.put("nome" , agenda.getNome());
        values.put("telefone" , agenda.getTelefone());
        values.put("celular" , agenda.getCelular());
        values.put("endereco" , agenda.getEndereço());


        getWritableDatabase().insert("Agenda",
                null ,
                values);
    }


    public List<Agenda> getLista() {
        List<Agenda> lista = new ArrayList<>();

        String colunas[] = {"id" , "nome" , "telefone" , "celular" , "endereco"} ;


        Cursor cursor =  getWritableDatabase().query(
                "Agenda",
                colunas,
                null,
                null,
                null,
                null,
                null);

        while(cursor.moveToNext()) {

            Agenda agenda = new Agenda();
            agenda.setId(cursor.getInt(0));
            agenda.setNome(cursor.getString(1));
            agenda.setTelefone(cursor.getString(2));
            agenda.setCelular(cursor.getString(3));
            agenda.setEndereço(cursor.getString(4));

            lista.add(agenda);

        }


        return lista ;


    }
}
