package br.com.senac.agendaaluno;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetalhesActivity extends AppCompatActivity {


    private Agenda agenda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes);

        Intent intent = getIntent();


        agenda = (Agenda) intent.getParcelableExtra(MainActivity.AGENDA);

        if (agenda != null) {

            TextView nome = findViewById(R.id.Nome);
            TextView telefone = findViewById(R.id.Telefone);
            TextView celularcontato = findViewById(R.id.Celular);
            TextView endereço = findViewById(R.id.Endereco);


            nome.setText(agenda.getNome());
            telefone.setText(agenda.getTelefone());
            endereço.setText(agenda.getEndereço());
            celularcontato.setText(agenda.getCelular());


        }
    }
}