package br.com.senac.agendaaluno;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NovoActivity extends AppCompatActivity {
    private EditText txtNome ;
    private EditText txtTelefone ;
    private EditText txtCelular ;
    private EditText txtEndereco ;
    private Button button ;

    private Agenda agenda ;
    private AgendaDAO dao ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo);

        Intent intent = getIntent();
        agenda = (Agenda)intent.getSerializableExtra(MainActivity.AGENDA);
        if (agenda==null){
            agenda = new Agenda();
        }else {
            preencherActivity(agenda);
        }




        txtNome = findViewById(R.id.txtNome);
        txtTelefone = findViewById(R.id.txtTelefone);
        txtCelular = findViewById(R.id.txtCelular);
        txtEndereco = findViewById(R.id.txtEndereco);
        button = findViewById(R.id.button);




        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                agenda = new Agenda();

                agenda.setNome(txtNome.getText().toString());
                agenda.setTelefone(txtTelefone.getText().toString());
                agenda.setCelular(txtCelular.getText().toString());
                agenda.setEndereço(txtEndereco.getText().toString());

                dao = new AgendaDAO(NovoActivity.this);
                dao.salvar(agenda);
                dao.close();
                Toast.makeText(NovoActivity.this ,
                        "Salvo com sucesso." ,
                        Toast.LENGTH_LONG).show();

                finish();

            }
        });


    }

    private void preencherActivity(Agenda agenda) {

        txtNome.setText(agenda.getNome());
        txtTelefone.setText(agenda.getEndereço());
        txtCelular.setText(agenda.getCelular());
        txtEndereco.setText(agenda.getEndereço());
    }


}


