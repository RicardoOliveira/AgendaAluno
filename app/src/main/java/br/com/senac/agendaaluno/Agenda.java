package br.com.senac.agendaaluno;

import java.io.Serializable;

/**
 * Created by sala304b on 12/03/2018.
 */

public class Agenda implements Serializable {


    private int id;
    private String nome ;
    private String telefone ;
    private String celular;
    private String endereço  ;



    public Agenda() {
    }

    public Agenda(String nome, String telefone, String celular, String endereço) {
        this.nome = nome;
        this.telefone = telefone;
        this.celular = celular ;
        this.endereço = endereço;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEndereço() {
        return endereço;
    }

    public void setEndereço(String endereço) {
        this.endereço = endereço;
    }

    @Override
    public String toString() {
        return this.nome ;
    }



}
