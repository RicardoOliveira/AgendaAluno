package br.com.senac.agendaaluno;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sala304b on 12/03/2018.
 */

public class AdapterAgenda extends BaseAdapter{

    private List<Agenda> lista;
    private Activity contexto;

    public AdapterAgenda(Activity context, List<Agenda> lista){
        this.contexto = contexto;
        this.lista = lista;

    }
    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int position) {
        return this.lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        int posicao = 0;
        for (int i = 0 ; i < this.lista.size();i++){
            if(this.lista.get(i).getId() == posicao){
                posicao = i ;
                break;
            }
        }
        return posicao;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = contexto.getLayoutInflater().inflate(R.layout.activity_list,parent,false);

        TextView textViewNome = view.findViewById(R.id.nome);


        Agenda contatos = this.lista.get(position);

        textViewNome.setText(contatos.getNome());

        return view;

    }














}
