package br.com.senac.agendaaluno;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;
import static br.com.senac.agendaaluno.R.id.ListaAgenda;

public class MainActivity extends AppCompatActivity {
    private static final int NOVO = 1;
    private ListView listView ;
    private ArrayAdapter<Agenda> contatosArrayAdapter ;
    private List<Agenda> lista ;
    private AgendaDAO dao ;

    private AdapterAgenda adapterAgenda;
    private ArrayAdapter<Agenda> adapter;

    public static final String AGENDA = "agenda" ;

    private Agenda agendaSelecionados;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listView = findViewById(ListaAgenda);
        adapterAgenda = new AdapterAgenda(this,lista);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View contexto, int posicao, long indice) {

                agendaSelecionados = (Agenda) adapter.getItemAtPosition(posicao);

                Intent intent = new Intent(MainActivity.this,DetalhesActivity.class);

                intent.putExtra(AGENDA, String.valueOf(agendaSelecionados));

                startActivity(intent);
            }

        });
        registerForContextMenu(listView);
    }

    @Override
    protected void onResume() {
        super.onResume();

        listView = findViewById(R.id.ListaAgenda);

        dao = new AgendaDAO(this) ;
        lista = dao.getLista() ;
        dao.close();

        adapter = new ArrayAdapter<Agenda>(this , android.R.layout.simple_list_item_1 , lista) ;

        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.novo:
                Intent intent = new Intent(MainActivity.this,NovoActivity.class);
                Log.i("#MENU", "novo ");
                break;

            case R.id.sobre:
                Log.i("#MENU", "sobre ");
                break;
        }
        return true;
    }

    public void novo(MenuItem item) {

        Intent intent = new Intent(MainActivity.this, NovoActivity.class);
        startActivityForResult(intent , NOVO);
    }

    public void sobre(MenuItem item) {
        System.out.println("testando");
    }



    }

